(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>  \n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n  \n<ion-content>\n  <div class=\"container\">\n    \n    <div class=\"logo\">\n      <h1> ChatApp </h1>\n    </div>\n\n    <div class=\"login\">\n        <h2> LOGIN </h2>\n    </div>\n    <br>\n    <br>\n    <div>\n        <form class=\"form\">\n            <input type=\"text\" id=\"Text0\" placeholder=\"Nome\" /><br />\n            <br>\n            <input type=\"text\" id=\"Text1\" placeholder=\"Sobrenome\" /><br />\n            <br>\n\n            <div>\n              <ion-button [routerLink]=\"['/mapa']\"  color=\"primary\" style=\"width: 100px;\">Entrar</ion-button>\n            </div>\n            \n        </form>\n    </div>\n  </div>  \n</ion-content>\n"

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '',
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                }
            ])
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  width: 450px;\n  height: 450px;\n  margin: auto;\n  padding: 48px 40px 36px;\n  border: 1px solid;\n  margin-top: 10%;\n  margin-bottom: 10%;\n}\n\n.logo {\n  text-align: center;\n}\n\n.login {\n  text-align: center;\n}\n\n.form {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FsdW5vL3Byb2ovZmluYWxtZW50ZS9zcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxZQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ0Q7O0FERUE7RUFDSSxrQkFBQTtBQ0NKOztBREVBO0VBQ0Msa0JBQUE7QUNDRDs7QURFQTtFQUNDLGtCQUFBO0FDQ0QiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG5cdHdpZHRoOiA0NTBweDsgXG5cdGhlaWdodDogNDUwcHg7IFxuXHRtYXJnaW46IGF1dG87IFxuXHRwYWRkaW5nOiA0OHB4IDQwcHggMzZweDsgXG5cdGJvcmRlcjogMXB4IHNvbGlkOyBcblx0bWFyZ2luLXRvcDogMTAlOyBcblx0bWFyZ2luLWJvdHRvbTogMTAlO1xufVxuXG4ubG9nbyB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubG9naW4ge1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb3JtIHtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuIiwiLmNvbnRhaW5lciB7XG4gIHdpZHRoOiA0NTBweDtcbiAgaGVpZ2h0OiA0NTBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiA0OHB4IDQwcHggMzZweDtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIG1hcmdpbi10b3A6IDEwJTtcbiAgbWFyZ2luLWJvdHRvbTogMTAlO1xufVxuXG4ubG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZ2luIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uZm9ybSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");




let HomePage = class HomePage {
    constructor(googleMaps, plt, nav, loadingCtrl, toastCtrl) {
        this.googleMaps = googleMaps;
        this.plt = plt;
        this.nav = nav;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        console.log('Construtor');
    }
    initMap() {
        console.log('InitMap Iniciando');
        this.map = this.googleMaps.create('map');
        this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MAP_READY).then((data) => {
            let coordinates = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](-2.549531, -44.240750);
            let position = {
                target: coordinates,
                zoom: 17
            };
            this.map.animateCamera(position);
            let markerOptions = {
                position: coordinates,
                icon: "assets/images/icons8-Marker-64.png",
                title: "CEST",
                snippet: 'http://www.cest.edu.br/',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP
            };
            const marker = this.map.addMarker(markerOptions).then((marker) => {
                marker.showInfoWindow();
            });
        });
    }
    ngAfterViewInit() {
        this.plt.ready().then(() => {
            this.initMap();
        });
    }
    onGPSClick() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.map.clear(); //limpa o mapa
            this.loading = yield this.loadingCtrl.create({
                message: 'Aguarde...'
            });
            //exibe a mensagem de carregamento
            yield this.loading.present();
            //local do dispositivo
            this.map.getMyLocation().then((location) => {
                this.loading.dismiss();
                console.log(JSON.stringify(location, null, 2));
                //move o mapa para o local determinado
                this.map.animateCamera({
                    target: location.latLng,
                    zoom: 17,
                    tilt: 30
                });
                //cria um marcador no mapa
                let marker = this.map.addMarkerSync({
                    title: 'Eu estou aqui',
                    snippet: 'Um subtitulo',
                    position: location.latLng,
                    animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].BOUNCE
                });
                //exibe o quadro de informacoes
                marker.showInfoWindow();
                //se for clicado exibe uma mendagem push
                marker.on(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MARKER_CLICK).subscribe(() => {
                    this.showToast('clicou');
                });
            }).catch(err => {
                //em saco de erro - fecha a tela de carregamento
                this.loading.dismiss();
                //exibe o push na tela - sobrepondo o app
                this.showToast(err.error_message);
            });
        });
    }
    //funcao de mensagem push
    showToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let toast = yield this.toastCtrl.create({
                message: message,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
        styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map