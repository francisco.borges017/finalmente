import { Component,ViewChild,AfterViewInit } from '@angular/core';
import { Platform, NavController, ToastController, LoadingController } from "@ionic/angular";
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    LatLng,
    MarkerOptions,
    Marker,
    GoogleMapsAnimation,
    MyLocation
} from "@ionic-native/google-maps";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements AfterViewInit {
    map: GoogleMap;
    loading: any;

    constructor(public googleMaps: GoogleMaps, public plt: Platform, public nav: NavController,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    console.log('Construtor');
    }


    initMap() {
        console.log('InitMap Iniciando');
        this.map = this.googleMaps.create('map');
        this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
            let coordinates: LatLng = new LatLng(-2.549531, -44.240750);
	    let position = {
                target: coordinates,
	        zoom: 17
	    };
            this.map.animateCamera(position);
	    let markerOptions: MarkerOptions = {
                position: coordinates,
	        icon: "assets/images/icons8-Marker-64.png",
	        title: "CEST",
	        snippet: 'http://www.cest.edu.br/',
	        animation: GoogleMapsAnimation.DROP
	    };
    	    const marker = this.map.addMarker(markerOptions).then((marker: Marker) => {
                marker.showInfoWindow();
	    });
       });
    }
    ngAfterViewInit() {
        this.plt.ready().then(() => {
            this.initMap();
        });
    }

    async onGPSClick() {
        this.map.clear(); //limpa o mapa
    
        this.loading = await this.loadingCtrl.create({
            message: 'Aguarde...'
        });
        //exibe a mensagem de carregamento
    
        await this.loading.present();

        //local do dispositivo
        this.map.getMyLocation().then((location: MyLocation) => {
            this.loading.dismiss();
	    console.log(JSON.stringify(location, null, 2));

	    //move o mapa para o local determinado
	    this.map.animateCamera({
	        target: location.latLng,
	        zoom: 17,
	        tilt: 30
	    });

	    //cria um marcador no mapa
	    let marker: Marker = this.map.addMarkerSync({
	        title: 'Eu estou aqui',
	        snippet: 'Um subtitulo',
	        position: location.latLng,
	        animation: GoogleMapsAnimation.BOUNCE
            });

	    //exibe o quadro de informacoes
	    marker.showInfoWindow();

	    //se for clicado exibe uma mendagem push
	    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
	        this.showToast('clicou');
	    });
        }).catch(err => {
            //em saco de erro - fecha a tela de carregamento
	    this.loading.dismiss();
	    //exibe o push na tela - sobrepondo o app
	    this.showToast(err.error_message);
        });
    }

    //funcao de mensagem push
    async showToast(message: string) {
        let toast = await this.toastCtrl.create({
        message: message,
        duration: 2000,
        position: 'middle'
        });
    
        toast.present();
    }
}
